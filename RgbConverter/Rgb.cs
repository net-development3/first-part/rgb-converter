﻿using System;

namespace RgbConverter
{
    public static class Rgb
    {
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            int[] colors = new int[] { red, green, blue };
            string result = string.Empty;

            for (int i = 0; i < colors.Length; i++)
            {
                if (colors[i] < 0)
                {
                    colors[i] = 0;
                }
                else if (colors[i] > 255)
                {
                    colors[i] = 255;
                }

                string colorHex = IntToHex(colors[i]);
                result += colorHex;
            }

            return result;
        }

        public static string IntToHex(int n)
        {
            char[] hexaNum = new char[2];
            int i = 0;
            string hexCode = string.Empty;

            while (n != 0)
            {
                int temp = 0;
                temp = n % 16;
                if (temp < 10)
                {
                    hexaNum[i] = (char)(temp + 48);
                    i++;
                }
                else
                {
                    hexaNum[i] = (char)(temp + 55);
                    i++;
                }

                n = n / 16;
            }

            if (i == 2)
            {
                hexCode += hexaNum[1];
                hexCode += hexaNum[0];
            }
            else if (i == 1)
            {
                hexCode = "0";
                hexCode += hexaNum[0];
            }
            else if (i == 0)
            {
                hexCode = "00";
            }

            return hexCode;
        }
    }
}
